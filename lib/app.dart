import 'view/pages/principal.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'view/pages/login.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _prefences = SharedPreferences.getInstance();
  Widget _init = const Scaffold(
    body: Center(child: CircularProgressIndicator()),
  );

  @override
  void initState() {
    super.initState();

    _prefences.then((preferences) {
      if (preferences.getString("uid") != null) {
        setState(() {
          _init = const PrincipalPage();
        });
      } else {
        setState(() {
          _init = LoginPage();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Control Finanzas",
      home: _init,
    );
  }
}
