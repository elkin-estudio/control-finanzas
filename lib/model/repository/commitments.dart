import 'package:flutter/foundation.dart';

import '/model/entity/commitments.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CommitmentsRepository {
  late final CollectionReference _collection;

  CommitmentsRepository() {
    _collection = FirebaseFirestore.instance.collection("commitments");
  }

  Future<void> newCommitments(CommitmentsEntity commitments) async {
    await _collection
        .withConverter<CommitmentsEntity>(
            fromFirestore: CommitmentsEntity.fromFirestore,
            toFirestore: ((value, _) => value.toFirestore()))
        .add(commitments);
  }

  Future<List<CommitmentsEntity>> getAllByUserId(String id) async {
    var query = await _collection
        .where("userId", isEqualTo: id)
        .withConverter<CommitmentsEntity>(
            fromFirestore: CommitmentsEntity.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var commitment = query.docs.cast().map<CommitmentsEntity>((e) {
      var commitment = e.data();
      commitment.id = e.id;
      return commitment;
    });

    return commitment.toList();
  }
}
