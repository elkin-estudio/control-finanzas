import 'package:cloud_firestore/cloud_firestore.dart';

import '../entity/savings.dart';

class SavingsRepository {
  late final CollectionReference _collection;

  SavingsRepository() {
    _collection = FirebaseFirestore.instance.collection("savings");
  }

  Future<void> newSavings(SavingsEntity savings) async {
    await _collection
        .withConverter(
            fromFirestore: SavingsEntity.fromFirestore,
            toFirestore: ((value, options) => value.toFirestore()))
        .add(savings);
  }

  Future<List<SavingsEntity>> getAllByUserId(String id) async {
    var query = await _collection
        .where("userId", isEqualTo: id)
        .withConverter<SavingsEntity>(
            fromFirestore: SavingsEntity.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var savings = query.docs.cast().map<SavingsEntity>((e) {
      var savings = e.data();
      savings.id = e.id;
      return savings;
    });

    return savings.toList();
  }
}
