import 'dart:convert';

import 'package:http/http.dart' as http;

class BackendRepository {
  Future<int> CalculatePeriodicity(String periodicity) async {
    var request = {
      "detalle": periodicity,
    };

    var url = Uri.https("control-finanzas-misiontics.herokuapp.com",
        "api/controlfinanzas/periodicidadPagoprincipal");
    var response = await http.post(url,
        headers: {'Content-Type': 'application/json'},
        body: json.encode(request));

    var body = json.decode(response.body);

    if (response.statusCode != 200) {
      return Future.error(body["message"]);
    }
    return body["periodicity"];
  }
}
