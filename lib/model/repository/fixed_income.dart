import 'package:app_control_finanzas/model/entity/fixed_income.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FixedIncomeRepository {
  late final CollectionReference _collection;

  FixedIncomeRepository() {
    _collection = FirebaseFirestore.instance.collection("principal_payment");
  }

  Future<void> newFixedIncome(FixedIncomeEntity fixedIncome) async {
    await _collection
        .withConverter(
            fromFirestore: FixedIncomeEntity.fromFirestore,
            toFirestore: ((value, options) => value.toFirestore()))
        .add(fixedIncome);
  }
}
