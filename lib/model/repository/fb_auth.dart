import 'package:firebase_auth/firebase_auth.dart';

class FirebaseAuthenticationRepository {
  Future<void> createEmailPasswordAccount(String email, String password) async {
    try {
      final credential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return Future.error('La contraseña es muy debil');
      } else if (e.code == 'email-already-in-use') {
        return Future.error('La cuenta de correo electronico ya existe');
      }
    }
  }

  Future<void> signInEmailPassword(String email, String password) async {
    try {
      final credential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return Future.error('El correo electronico no se encuentra registrado');
      } else if (e.code == 'wrong-password') {
        print('Credenciales invalidas');
      }
    }
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }
}
