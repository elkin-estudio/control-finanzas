import 'package:cloud_firestore/cloud_firestore.dart';

class SavingsEntity {
  late String? id;
  late String? userId;
  late String? details;
  late String? paymentPeriodicity;
  late double? saving;
  late String? initialDate;
  late int? numberInstallment;
  late String? endDate;
  late double? goal;

  SavingsEntity({
    this.userId,
    this.details,
    this.paymentPeriodicity,
    this.saving,
    this.initialDate,
    this.numberInstallment,
    this.endDate,
    this.goal,
  });

  factory SavingsEntity.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    var data = snapshot.data();
    return SavingsEntity(
      userId: data?["userId"],
      details: data?["details"],
      paymentPeriodicity: data?["paymentPeriodicity"],
      saving: data?["saving"],
      initialDate: data?["initialDate"],
      numberInstallment: data?["numberInstallment"],
      endDate: data?["endDate"],
      goal: data?["goal"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (userId != null && userId!.isNotEmpty) "userId": userId,
      if (details != null && details!.isNotEmpty) "details": details,
      if (paymentPeriodicity != null && paymentPeriodicity!.isNotEmpty)
        "paymentPeriodicity": paymentPeriodicity,
      if (saving != null) "saving": saving,
      if (initialDate != null && initialDate!.isNotEmpty)
        "initialDate": initialDate,
      if (numberInstallment != null) "numberInstallment": numberInstallment,
      if (endDate != null && endDate!.isNotEmpty) "endDate": endDate,
      if (goal != null) "goal": goal,
    };
  }
}
