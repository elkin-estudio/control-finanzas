import 'package:cloud_firestore/cloud_firestore.dart';

class UserEntity {
  late String? id;
  late String? name;
  late String? lastName;
  late String? email;
  late String? photo;
  //late String? password;
  late String? country;
  late String? address;
  late String? indicativePhone;
  late String? phone;
  late String? gender;
  late String? birthDay;

  UserEntity(
      {this.name,
      this.lastName,
      this.email,
      this.photo,
      //this.password,
      this.country,
      this.address,
      this.indicativePhone,
      this.phone,
      this.gender,
      this.birthDay});

  factory UserEntity.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    var data = snapshot.data();
    return UserEntity(
      name: data?["name"],
      lastName: data?["lastname"],
      email: data?["email"],
      country: data?["country"],
      address: data?["address"],
      indicativePhone: data?["indicativePhone"],
      photo: data?["photo"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (name != null && name!.isNotEmpty) "name": name,
      if (lastName != null && lastName!.isNotEmpty) "lastName": lastName,
      if (email != null && email!.isNotEmpty) "email": email,
      if (country != null && country!.isNotEmpty) "country": country,
      if (address != null && address!.isNotEmpty) "address": address,
      if (indicativePhone != null && indicativePhone!.isNotEmpty)
        "indicativePhone": indicativePhone,
      if (photo != null && photo!.isNotEmpty) "photo": photo,
      if (gender != null && gender!.isNotEmpty) "gender": gender,
      if (birthDay != null && birthDay!.isNotEmpty) "birthDay": birthDay,
    };
  }

  @override
  String toString() {
    // TODO: implement toString
    return "UserEntity {$name,$lastName,$email,$country,$address,$indicativePhone,$phone,$gender,$birthDay}";
  }
}
