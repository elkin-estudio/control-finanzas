import 'package:cloud_firestore/cloud_firestore.dart';

class FixedIncomeEntity {
  late String? userId;
  late String? paymentPeriodicity;
  late double? consignedValue;
  late String? detail;
  late int? peridocity;

  FixedIncomeEntity({
    this.userId,
    this.paymentPeriodicity,
    this.consignedValue,
    this.detail,
    this.peridocity,
  });

  factory FixedIncomeEntity.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    var data = snapshot.data();
    return FixedIncomeEntity(
      userId: data?["userId"],
      paymentPeriodicity: data?["paymentPeriodicity"],
      consignedValue: data?["consignedValue"],
      detail: data?["detail"],
      peridocity: data?["peridocity"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (userId != null && userId!.isNotEmpty) "userId": userId,
      if (paymentPeriodicity != null && paymentPeriodicity!.isNotEmpty)
        "paymentPeriodicity": paymentPeriodicity,
      if (consignedValue != null) "value": consignedValue,
      if (detail != null && detail!.isNotEmpty) "detail": detail,
      if (peridocity != null) "peridocity": peridocity,
    };
  }
}
