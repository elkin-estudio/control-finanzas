import 'package:cloud_firestore/cloud_firestore.dart';

class CommitmentsEntity {
  late String? id;
  late String? userId;
  late String? detail;
  late double? commitmentValue;
  late String? valueType;
  late String? courtDate;
  late bool? paymentEndDate;
  late int? numberInstallments;
  late String? finalDate;

  CommitmentsEntity({
    this.userId,
    this.detail,
    this.commitmentValue,
    this.valueType,
    this.courtDate,
    this.paymentEndDate,
    this.numberInstallments,
    this.finalDate,
  });

  factory CommitmentsEntity.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    var data = snapshot.data();
    return CommitmentsEntity(
      userId: data?["userId"],
      detail: data?["detail"],
      commitmentValue: data?["commitmentValue"],
      valueType: data?["valueType"],
      courtDate: data?["courtDate"],
      paymentEndDate: data?["paymentEndDate"],
      numberInstallments: data?["numberInstallments"],
      finalDate: data?["finalDate"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (userId != null && userId!.isNotEmpty) "userId": userId,
      if (detail != null && detail!.isNotEmpty) "detail": detail,
      if (commitmentValue != null) "commitmentValue": commitmentValue,
      if (valueType != null && valueType!.isNotEmpty) "valueType": valueType,
      if (courtDate != null && courtDate!.isNotEmpty) "courtDate": courtDate,
      if (paymentEndDate != null) "paymentEndDate": paymentEndDate,
      if (numberInstallments != null) "numberInstallments": numberInstallments,
      if (finalDate != null && finalDate!.isNotEmpty) "finalDate": finalDate,
    };
  }
}
