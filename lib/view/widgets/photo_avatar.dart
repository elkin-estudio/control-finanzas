import 'package:flutter/material.dart';

class photoAvatarWidget extends StatefulWidget {
  const photoAvatarWidget({super.key});

  @override
  State<photoAvatarWidget> createState() => _photoAvatarWidgetState();
}

class _photoAvatarWidgetState extends State<photoAvatarWidget> {
  @override
  Widget build(BuildContext context) {
    return const Icon(Icons.camera_enhance_outlined);
  }
}
