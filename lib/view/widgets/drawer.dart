import 'dart:io';

import 'package:app_control_finanzas/controller/login.dart';
import 'package:app_control_finanzas/view/pages/loans_made.dart';
import 'package:app_control_finanzas/view/pages/modify_account.dart';
import 'package:app_control_finanzas/view/pages/principal.dart';
import 'package:app_control_finanzas/view/pages/principal_saving.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/view/pages/requested_loan.dart';

import '/view/pages/additional_payments.dart';
import '/view/pages/credits.dart';

import '/view/pages/register_commitments.dart';
import 'package:flutter/material.dart';

import '../pages/about.dart';
import '/view/pages/login.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({super.key});

  @override
  State<DrawerWidget> createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final _preferences = SharedPreferences.getInstance();
  final _loginController = LoginController();
  String _name = "";
  String _email = "";
  String _photo = "";

  @override
  void initState() {
    super.initState();

    _preferences.then((preferences) {
      setState(() {
        _email = preferences.getString("email") ?? "N/A";
        _name = preferences.getString("name") ?? "N/A";
        _photo = preferences.getString("photo") ?? "N/A";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Color.fromARGB(255, 0, 116, 170),
            ),
            child: _header(),
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.account_balance),
              title: Text('Pantalla principal'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => const PrincipalPage(),
                ),
              );
            },
          ),
          /*GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.account_balance),
              title: Text('Registro pago principal'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => principalPaymentPage(),
                ),
              );
            },
          ),*/
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.account_balance_wallet),
              title: Text('Registro compromisos'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RegisterCommitmentsPage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.account_tree),
              title: Text('Registro pagos adicionales'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const AdditionalPaymentsPage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.credit_card),
              title: Text('Registro creditos'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const CreditsPage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.savings),
              title: Text('Registro ahorros'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const PrincipalSavingPage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.credit_card_outlined),
              title: Text('Prestamos solicitados'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const RequestedLoanPage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.credit_card_off),
              title: Text('Prestamos realizados'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const LoansMadePage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.account_circle),
              title: Text('Cambiar informacion personal'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ModifyAccountPage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.account_circle),
              title: Text('Acerca de...'),
            ),
            onTap: () {
              // Validar usuario y contraseña en base de datos
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const AboutPage(),
                ),
              );
            },
          ),
          GestureDetector(
            child: const ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Cerrar Sesion'),
            ),
            onTap: () async {
              var nav = Navigator.of(context);

              // Cerramos sesion en Auth de Firebase
              _loginController.logout();

              // limpiar las preferences
              var preferences = await _preferences;
              preferences.remove("uid");
              preferences.remove("email");
              preferences.remove("name");
              preferences.remove("photo");

              // Volver a la pagina de login
              nav.pushReplacement(MaterialPageRoute(
                builder: (context) => LoginPage(),
              ));
            },
          ),
        ],
      ),
    );
  }

  Widget _header() {
    // Consultar Datos Cabecera
    const nameApp = "Control Finanzas";
    const image = Icon(Icons.manage_accounts);

    if (_photo == null || _photo == "N/A") {
      return Row(
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            const Text(nameApp,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 24)),
            const SizedBox(
              height: 16,
            ),
            Row(children: [
              const CircleAvatar(
                radius: 30,
                child: image,
                //backgroundImage: image,
                //backgroundImage: FileImage(File(_photo)),
              ),
              const SizedBox(
                width: 16,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(_name,
                        style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16)),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(_email,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 14))
                  ])
            ]),
          ]),
        ],
      );
    } else {
      return Row(
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            const Text(nameApp,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 24)),
            const SizedBox(
              height: 16,
            ),
            Row(children: [
              CircleAvatar(
                radius: 30,
                backgroundImage: FileImage(File(_photo)),
              ),
              const SizedBox(
                width: 16,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(_name,
                        style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16)),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(_email,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 14))
                  ])
            ]),
          ]),
        ],
      );
    }
  }
}
