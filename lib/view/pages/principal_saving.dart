import 'package:app_control_finanzas/view/pages/savings.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../controller/savings.dart';
import '../../model/entity/savings.dart';
import '../widgets/drawer.dart';

class PrincipalSavingPage extends StatefulWidget {
  const PrincipalSavingPage({super.key});

  @override
  State<PrincipalSavingPage> createState() => _PrincipalSavingPageState();
}

class _PrincipalSavingPageState extends State<PrincipalSavingPage> {
  List<SavingsEntity> _list = [];
  final _preferences = SharedPreferences.getInstance();
  final _savingsController = SavingsController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _listAllSavings();
  }

  void _listAllSavings() {
    _preferences.then((preferences) {
      var id = preferences.getString("uid") ?? "";
      setState(() {
        _savingsController.listAll(id).then((value) {
          setState(() {
            _list = value;
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      body: Center(
        child: Column(children: <Widget>[
          AppBar(
            centerTitle: true,
            title: const Text("Control Finanzas"),
            backgroundColor: const Color.fromARGB(255, 0, 116, 170),
          ),
          _lastPaymentsDate(),
          _payments(),
          const SizedBox(
            height: 30,
          ),
          _listHead(),
          _listSavings(),
          const SizedBox(
            height: 50,
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                      color: Colors.transparent,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const <Widget>[
                            Text("Ahorro Actual : ",
                                style: TextStyle(
                                  fontSize: 20,
                                )),
                          ]))),
              Expanded(
                  child: Container(
                      color: Colors.transparent,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const <Widget>[
                            Text("\$1.420.000",
                                style: TextStyle(
                                    fontSize: 25, fontWeight: FontWeight.bold)),
                          ]))),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              alignment: Alignment.topRight,
              child: FloatingActionButton(
                child: const Icon(Icons.savings),
                onPressed: () async {
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SavingsPage(),
                    ),
                  );

                  if (!mounted) return;

                  //_listAllCommitments();
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Widget _lastPaymentsDate() {
    return Column(
      children: const [
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              hintText: 'Fecha ultimo Ahorro',
            )),
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 20),
                hintText: '15/Noviembre/2022')),
      ],
    );
  }

  Widget _payments() {
    return Column(
      children: const [
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                hintText: 'Ahorro')),
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 30),
                hintText: '\$50.000')),
      ],
    );
  }

  Widget _listHead() {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: SizedBox(
              width: 500,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    'Descripcion'),
              ),
            ),
          ),
          Flexible(
            child: SizedBox(
              width: 150,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    'Monto'),
              ),
            ),
          ),
          Flexible(
            child: SizedBox(
              width: 150,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.right,
                    'Ver'),
              ),
            ),
          ),
        ],
      )
    ]);
  }

  Widget _listSavings() {
    return Expanded(
        child: ListView.builder(
      itemCount: _list.length,
      itemBuilder: (context, index) => Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: SizedBox(
                width: 600,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                      style: const TextStyle(
                          fontSize: 14, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                      _list[index].details!),
                ),
              ),
            ),
            Flexible(
              child: SizedBox(
                width: 150,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                      style: const TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                      _list[index].saving!.toString()),
                ),
              ),
            ),
            Flexible(
              child: SizedBox(
                width: 100,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    icon: const Icon(Icons.view_list),
                    onPressed: () {
                      // TODO Realizar la llamada Telefonica
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 15,
        ),
      ]),
    ));
  }
}
