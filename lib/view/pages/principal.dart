import 'package:app_control_finanzas/controller/commitments.dart';
import 'package:app_control_finanzas/model/entity/commitments.dart';
import 'package:app_control_finanzas/view/pages/register_commitments.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/drawer.dart';

const List<String> listEstado = <String>['Si', 'No'];

class PrincipalPage extends StatefulWidget {
  const PrincipalPage({super.key});

  @override
  State<PrincipalPage> createState() => _PrincipalPageState();
}

class _PrincipalPageState extends State<PrincipalPage> {
  List<CommitmentsEntity> _list = [];
  final _preferences = SharedPreferences.getInstance();
  final _commitmentsController = CommitmentsController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _listAllCommitments();
  }

  void _listAllCommitments() {
    _preferences.then((preferences) {
      var id = preferences.getString("uid") ?? "";
      setState(() {
        _commitmentsController.listAll(id).then((value) {
          setState(() {
            _list = value;
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      body: Center(
        child: Column(children: <Widget>[
          AppBar(
            centerTitle: true,
            title: const Text("Control Finanzas"),
            backgroundColor: const Color.fromARGB(255, 0, 116, 170),
          ),
          _lastPaymentsDate(),
          _payments(),
          const SizedBox(
            height: 30,
          ),
          _listHead(),

          _listCommitments(),

          const SizedBox(
            height: 50,
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                      color: Colors.transparent,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const <Widget>[
                            Text("Dinero Actual : ",
                                style: TextStyle(
                                  fontSize: 20,
                                )),
                          ]))),
              Expanded(
                  child: Container(
                      color: Colors.transparent,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const <Widget>[
                            Text("\$420.000",
                                style: TextStyle(
                                    fontSize: 25, fontWeight: FontWeight.bold)),
                          ]))),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                      color: Colors.transparent,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const <Widget>[
                            Text("Dinero Libre : ",
                                style: TextStyle(
                                  fontSize: 20,
                                )),
                          ]))),
              Expanded(
                  child: Container(
                      color: Colors.transparent,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const <Widget>[
                            Text("60.000",
                                style: TextStyle(
                                    fontSize: 25, fontWeight: FontWeight.bold)),
                          ]))),
            ],
          ), // Tex // Text Top

          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              alignment: Alignment.topRight,
              child: FloatingActionButton(
                child: const Icon(Icons.add_card_rounded),
                onPressed: () async {
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RegisterCommitmentsPage(),
                    ),
                  );

                  if (!mounted) return;

                  //_listAllCommitments();
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Widget _lastPaymentsDate() {
    return Column(
      children: const [
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              hintText: 'Fecha ultimo pago',
            )),
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 20),
                hintText: '15/Octubre/2022')),
      ],
    );
  }

  Widget _payments() {
    return Column(
      children: const [
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                hintText: 'Pago')),
        TextField(
            enabled: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 30),
                hintText: '\$620.000')),
      ],
    );
  }

  Widget _listHead() {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: SizedBox(
              width: 500,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    'Descripcion'),
              ),
            ),
          ),
          Flexible(
            child: SizedBox(
              width: 150,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    'Monto'),
              ),
            ),
          ),
          Flexible(
            child: SizedBox(
              width: 150,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    'Pague'),
              ),
            ),
          ),
          Flexible(
            child: SizedBox(
              width: 150,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    'Valor Pagado'),
              ),
            ),
          ),
          Flexible(
            child: SizedBox(
              width: 150,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: const Text(
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.right,
                    'Ver'),
              ),
            ),
          ),
        ],
      )
    ]);
  }

  Widget _listCommitments() {
    return Expanded(
        child: ListView.builder(
      itemCount: _list.length,
      itemBuilder: (context, index) => Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: SizedBox(
                width: 500,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                      style: const TextStyle(
                          fontSize: 14, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                      _list[index].detail!),
                ),
              ),
            ),
            Flexible(
              child: SizedBox(
                width: 150,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                      style: const TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                      _list[index].commitmentValue!.toString()),
                ),
              ),
            ),
            Flexible(
              child: SizedBox(
                width: 100,
                child: Container(
                  alignment: Alignment.center,
                  child: const Text(
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                      "Si"),
                ),
              ),
            ),
            Flexible(
              child: SizedBox(
                width: 100,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                      style: const TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                      _list[index].commitmentValue!.toString()),
                ),
              ),
            ),
            Flexible(
              child: SizedBox(
                width: 100,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    icon: const Icon(Icons.view_list),
                    onPressed: () {
                      // TODO Realizar la llamada Telefonica
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 15,
        ),
      ]),
    ));
  }

  /*Widget _listCommitments() {
    return Expanded(
      child: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (context, index) => ListTile(
          leading: const CircleAvatar(),
          title: Text(_list[index].detail!),
          subtitle: Text(_list[index].id!),
          trailing: IconButton(
            icon: const Icon(Icons.phone),
            onPressed: () {
              // TODO Realizar la llamada Telefonica
            },
          ),
          onTap: () {
            // TODO: Ir a la ventana detalle del cobro
          },
        ),
      ),
    );
  }*/

}

class DropdownButtonEstado extends StatefulWidget {
  const DropdownButtonEstado({super.key});

  @override
  State<DropdownButtonEstado> createState() => _DropdownButtonEstadoState();
}

class _DropdownButtonEstadoState extends State<DropdownButtonEstado> {
  String dropdownValue = listEstado.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: const TextStyle(color: Color.fromARGB(255, 8, 32, 139)),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? value) {
        // This is called when the user selects an item.
        setState(() {
          dropdownValue = value!;
        });
      },
      items: listEstado.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
