import '/controller/login.dart';
import '/controller/request/register.dart';
import '/view/widgets/drawer.dart';

//import '/view/widgets/date_picker.dart';
import 'package:flutter/material.dart';

const List<String> listGenero = <String>[
  'Masculino',
  'Femenino',
  'Prefieron no decirlo'
];

class ModifyAccountPage extends StatelessWidget {
  late RegisterRequest _data;
  late LoginController _controller;

  ModifyAccountPage({super.key}) {
    _data = RegisterRequest();
    _controller = LoginController();
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    return Scaffold(
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AppBar(
              centerTitle: true,
              title: const Text("Control Finanzas"),
              backgroundColor: const Color.fromARGB(255, 0, 116, 170),
            ),
            const SizedBox(
              height: 40,
            ),
            const Text(
                style: TextStyle(fontSize: 36),
                textAlign: TextAlign.left,
                'Modificar Cuenta'),
            Padding(
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  children: [
                    _form(context),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<
        FormState>(); // registramos el formulario en flutter para Habilitar validaciones especiales
    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            _fieldName(),
            _fieldLastName(),
            _fieldEmailAddress(),
            _fieldPassword(),
            const SizedBox(
              height: 25,
            ),
            _fieldCountry(),
            _address(),
            _fieldNumberPhone(),
            const SizedBox(
              height: 25,
            ),
            _fieldGender(),
            const SizedBox(
              height: 50,
            ),
            _fieldDate(),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              child: const Text("Modificar Cuenta",
                  style: TextStyle(fontSize: 24)),
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  // Validar usuario y contraseña en base de datos
                  try {
                    await _controller.registerNewUser(_data);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text("Usuario modificado correctamente")));
                  } catch (error) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text(error.toString())));
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _fieldName() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Nombre'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El campo nombre es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            _data.name = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldLastName() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Apellidos'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El campo apellidos es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            _data.lastName = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldEmailAddress() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Correo Electronico'),
        ),
        TextFormField(
          enabled: false,
          maxLength: 30,
          keyboardType: TextInputType.emailAddress,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El correo electronico es obligatorio";
            }
            if ((!value.contains("@")) || (!value.contains("."))) {
              return "El correo electronico no tiene un formato valido";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            _data.email = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldPassword() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Contraseña'),
        ),
        TextFormField(
          obscureText: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "La contraseña es obligatoria";
            }
            if (value.length < 8) {
              return "Minimo debe contener 8 caracteres";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            _data.password = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldCountry() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Pais'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El campo pais es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            _data.country = value!;
          },
        ),
      ],
    );
  }

  Widget _address() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Direccion'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onSaved: (value) {
            _data.address = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldNumberPhone() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: SizedBox(
                width: 100,
                child: Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.start,
                      'Indicativo'),
                ),
              ),
            ),
            const SizedBox(
              width: 25,
            ),
            Flexible(
              child: Container(
                alignment: Alignment.bottomLeft,
                child: const Text(
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.start,
                    'Telefono'),
              ),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: SizedBox(
                width: 100,
                child: TextFormField(
                  maxLength: 10,
                  obscureText: false,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "+57"),
                  validator: (value) {},
                  onSaved: (value) {
                    _data.indicativePhone = value!;
                  },
                ),
              ),
            ),
            const SizedBox(
              width: 25,
            ),
            Flexible(
              child: TextFormField(
                maxLength: 20,
                obscureText: false,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: "Telefono"),
                validator: (value) {},
                onSaved: (value) {
                  _data.phone = value!;
                },
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget _fieldGender() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Genero'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onSaved: (value) {
            _data.gender = value!;
          },
        ),
      ],
    );
  }

  /*Widget _fieldGender() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Genero:'),
        ),
        Container(
            alignment: Alignment.bottomLeft,
            child: const DropdownButtonGenero()),
      ],
    );
  }*/

  Widget _fieldDate() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Fecha Nacimiento'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onSaved: (value) {
            _data.birthDay = value!;
          },
        ),
      ],
    );
  }

  /*Widget _fieldDate() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: 
          
          const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Fecha Nacimiento'),
        ),
        Container(alignment: Alignment.bottomLeft, 
        child: const DatePicker()
        
        ),
      ],
    );
  }*/
}

/////////// SELECCION
class DropdownButtonGenero extends StatefulWidget {
  const DropdownButtonGenero({super.key});

  @override
  State<DropdownButtonGenero> createState() => _DropdownButtonGeneroState();
}

class _DropdownButtonGeneroState extends State<DropdownButtonGenero> {
  String dropdownValue = listGenero.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: const TextStyle(fontSize: 25, color: Color.fromARGB(255, 0, 0, 0)),
      onChanged: (String? value) {
        // This is called when the user selects an item.
        setState(() {
          dropdownValue = value!;
        });
      },
      items: listGenero.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
