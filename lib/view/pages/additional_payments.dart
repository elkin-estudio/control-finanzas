import '/view/widgets/drawer.dart';
import 'package:flutter/material.dart';

const List<String> listDetalle = <String>[
  'Cuadros en Oleo',
  'Figuras 3D',
  'Pinturas',
  'Pocillos',
  'Lamparas'
];

const List<String> listTipoIngreso = <String>[
  'Fijo',
  'Variable',
];

class AdditionalPaymentsPage extends StatelessWidget {
  const AdditionalPaymentsPage(
      {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          AppBar(
            centerTitle: true,
            title: const Text("Control Finanzas"),
            backgroundColor: const Color.fromARGB(255, 0, 116, 170),
          ),
          Padding(
              padding: const EdgeInsets.all(40.0),
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                      style: TextStyle(fontSize: 36),
                      textAlign: TextAlign.left,
                      'Ingresos Adicionales'),
                  const SizedBox(
                    height: 20,
                  ),
                  _paymentDetail(),
                  _fieldPaymentConsigned(),
                  _valueType(),
                  const SizedBox(
                    height: 20,
                  ),
                  _fieldDateInitial(),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: const [
                      MyStatefulWidget(),
                      Text(
                          style: TextStyle(fontSize: 22),
                          textAlign: TextAlign.center,
                          'Fecha Final'),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  _fieldNumberInstallments(),
                  _fieldDateFinal(),
                  _fieldTotal(),
                ],
              )),
          Padding(
              padding: const EdgeInsets.all(0.0),
              child: Column(children: [
                ElevatedButton(
                  child:
                      const Text("  Nuevo   ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text("Modificar", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text(" Eliminar  ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text(" Guardar  ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
              ]))
        ]),
      ),
    );
  }

  Widget _paymentDetail() {
    return Column(children: [
      const TextField(
          enabled: false,
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            hintText: 'Detalle:',
          )),
      Container(
          alignment: Alignment.bottomLeft,
          child: const DropdownButtonlistDetalle()),
    ]);
  }

  Widget _fieldPaymentConsigned() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Valor:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _valueType() {
    return Column(children: [
      const TextField(
          enabled: false,
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            hintText: 'Tipo de Valor:',
          )),
      Container(
          alignment: Alignment.bottomLeft,
          child: const DropdownButtonlistTipoIngreso()),
    ]);
  }

  Widget _fieldDateInitial() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Fecha de corte'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onSaved: (value) {},
        ),
      ],
    );
  }

  Widget _fieldNumberInstallments() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Numero de Cuotas:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _fieldDateFinal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Fecha Final:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _fieldTotal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Total:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }
}

class DropdownButtonlistDetalle extends StatefulWidget {
  const DropdownButtonlistDetalle({super.key});

  @override
  State<DropdownButtonlistDetalle> createState() =>
      _DropdownButtonlistDetalleState();
}

class _DropdownButtonlistDetalleState extends State<DropdownButtonlistDetalle> {
  String dropdownValue = listDetalle.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: const TextStyle(
          color: Color.fromARGB(255, 0, 0, 0),
          fontSize: 28,
          fontWeight: FontWeight.bold),
      underline: Container(
        height: 0,
        color: const Color.fromARGB(255, 0, 34, 145),
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownValue = value!;
        });
      },
      items: listDetalle.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}

class DropdownButtonlistTipoIngreso extends StatefulWidget {
  const DropdownButtonlistTipoIngreso({super.key});

  @override
  State<DropdownButtonlistTipoIngreso> createState() =>
      _DropdownButtonlistTipoIngresoState();
}

class _DropdownButtonlistTipoIngresoState
    extends State<DropdownButtonlistTipoIngreso> {
  String dropdownValue = listTipoIngreso.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: const TextStyle(
          color: Color.fromARGB(255, 0, 0, 0),
          fontSize: 28,
          fontWeight: FontWeight.bold),
      underline: Container(
        height: 0,
        color: const Color.fromARGB(255, 0, 34, 145),
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownValue = value!;
        });
      },
      items: listTipoIngreso.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}

///// Checkbox
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({super.key});

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return const Color.fromARGB(255, 24, 0, 245);
    }

    return Checkbox(
      checkColor: Colors.white,
      fillColor: MaterialStateProperty.resolveWith(getColor),
      value: isChecked,
      onChanged: (bool? value) {
        setState(() {
          isChecked = value!;
        });
      },
    );
  }
}
