import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

class AboutPage extends StatelessWidget {

  const AboutPage({super.key});

  final _imageUrl = "assets/images/CFinanzas.jpg";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(

      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AppBar(
              centerTitle: true,
              title: const Text("Acerca de..."),
              backgroundColor: const Color.fromARGB(255, 0, 116, 170),
            ),
            _logo(),
            _form(),
          ],
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        Image.asset(_imageUrl), //Importamos,
        const SizedBox(
          height: 25,
        ),
      ],
    );
  }

  Widget _form() {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: Column(
        children: [
          const Text(
              style: TextStyle(fontSize: 22),
              textAlign: TextAlign.center,
              'Controla tus finanzas, no dejes que ellas te controlen a ti y coge las riendas de tu vida'),
          const SizedBox(
            height: 60,
          ),
          const Text(
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
              ' Recuerda :'),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                    '1. Revisa en familia los gastos del hogar e invitala a ahorrar',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                    style: TextStyle(fontSize: 16),
                    '2. Reevalúa la importancia de algunos gastos',
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                    style: TextStyle(fontSize: 16),
                    '3. Reserva dinero para pagar las deudas',
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                    style: TextStyle(fontSize: 16),
                    '4. Examina tu endeudamiento y toma desiciones',
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                    style: TextStyle(fontSize: 16),
                    '5. Genera ingresos Extras',
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                    style: TextStyle(fontSize: 16),
                    '6. Se precavido cuando tengas mas ingresos',
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 60,
          ),
          const Text(
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              'Desarrollado Por:'),
          const Text(style: TextStyle(fontSize: 14), 'Elkin A. Ocampo'),
          const SizedBox(
            height: 30,
          ),
          const Text(
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              'Numero de contacto:'),
          const Text(style: TextStyle(fontSize: 14), '(+57) 302-347-4167'),
          const SizedBox(
            height: 30,
          ),
          const Text(
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              'Correo Electronico:'),
          const Text(
              style: TextStyle(fontSize: 14), 'elkinocampo1053@gmail.com'),
        ],
      ),
    );
  }
}
