import 'package:flutter/material.dart';

class RecoverAccount extends StatelessWidget {
  final _imageUrl = "assets/images/CFinanzas.jpg";

  const RecoverAccount({super.key});

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            AppBar(
              centerTitle: true,
              title: const Text("Control Finanzas"),
              backgroundColor: const Color.fromARGB(255, 0, 116, 170),
            ),
            _logo(),
            Padding(
                padding: const EdgeInsets.all(40.0),
                child: Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                          style: TextStyle(fontSize: 36),
                          textAlign: TextAlign.left,
                          'Recuperar Cuenta'),
                      const SizedBox(
                        height: 50,
                      ),
                      const Text(
                          style: TextStyle(fontSize: 22),
                          textAlign: TextAlign.left,
                          'Favor ingrese el correo electronico de su cuenta para enviar los pasos necesarios para reestablecer su contraseña'),
                      const SizedBox(
                        height: 50,
                      ),
                      _form(context),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        Image.asset(_imageUrl), //Importamos,
        const SizedBox(
          height: 25,
        ),
      ],
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<
        FormState>(); // registramos el formulario en flutter para Habilitar validaciones especiales
    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          children: [
            _fieldEmailAddress(),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              child: const Text("Reestablecer Contraseña",
                  style: TextStyle(fontSize: 24)),
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  // Validar usuario y contraseña en base de datos
                  try {
                    //await _controller.registerNewUser(_data);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text("Usuario registrado correctamente")));
                  } catch (error) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text(error.toString())));
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _fieldEmailAddress() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Correo Electronico'),
        ),
        TextFormField(
          maxLength: 30,
          keyboardType: TextInputType.emailAddress,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El correo electronico es obligatorio";
            }
            if ((!value.contains("@")) || (!value.contains("."))) {
              return "El correo electronico no tiene un formato valido";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            //_data.email = value!;
          },
        ),
      ],
    );
  }
}
