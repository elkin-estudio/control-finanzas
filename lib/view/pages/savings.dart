import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/savings.dart';
import '../../model/entity/savings.dart';
import '/view/widgets/drawer.dart';
import 'package:flutter/material.dart';

/*const List<String> listDetalle = <String>[
  'Viaje a San Andres',
  'Nueva Lavadora',
];*/

const List<String> listTipoIngreso = <String>[
  'Fijo',
  'Variable',
];

class SavingsPage extends StatefulWidget {
  late final SavingsEntity _savings;
  late final SavingsController _controller;

  //late final _txtPeriodicidad = TextEditingController(text: "0");

  SavingsPage({super.key}) {
    _savings = SavingsEntity();
    _controller = SavingsController();
  }

  @override
  State<SavingsPage> createState() => _SavingsPageState();
}

class _SavingsPageState extends State<SavingsPage> {
  final _preferences = SharedPreferences.getInstance();
  String _userId = "";

  @override
  void initState() {
    super.initState();

    _preferences.then((preferences) {
      setState(() {
        _userId = preferences.getString("uid") ?? "N/A";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 2),
          child: Column(children: <Widget>[
            AppBar(
              centerTitle: true,
              title: const Text("Control Finanzas"),
              backgroundColor: const Color.fromARGB(255, 0, 116, 170),
            ),
            const SizedBox(
              height: 50,
            ),
            const Text(
                style: TextStyle(fontSize: 36),
                textAlign: TextAlign.left,
                'Ahorros'),
            Padding(
              padding: const EdgeInsets.all(40.0),
              child: Column(
                children: [
                  _form(context),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _paymentDetail(),
          _fieldPrice(),
          //_valueType(),
          const SizedBox(
            height: 20,
          ),
          _fieldDateInitial(),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: const [
              //MyStatefulWidget(),
              Text(
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                  'Fecha Final'),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          _fieldNumberInstallments(),
          _fieldDateFinal(),
          _fieldGoal(),
          _guardar(context, formKey),
        ],
      ),
    );
  }

  /*Widget _paymentDetail() {
    return Column(children: [
      const TextField(
          enabled: false,
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            hintText: 'Detalle:',
          )),
      Container(
          alignment: Alignment.bottomLeft,
          child: const DropdownButtonlistDetalle()),
    ]);
  }*/
  Widget _paymentDetail() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Detalle:',
            )),
        TextFormField(
          maxLength: 20,
          keyboardType: TextInputType.text,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: ""),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor del detalle es obligatorio";
            } else {
              return null;
            }
          },
          onChanged: (value) {
            widget._savings.details = value;
          },
        ),
      ],
    );
  }

  Widget _fieldPrice() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Ahorro:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor del ahorro es obligatorio";
            } else {
              return null;
            }
          },
          onChanged: (value) {
            widget._savings.saving = double.parse(value);
          },
        ),
      ],
    );
  }

  /*Widget _valueType() {
    return Column(children: [
      const TextField(
          enabled: false,
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            hintText: 'Tipo de Valor:',
          )),
      Container(
          alignment: Alignment.bottomLeft,
          child: const DropdownButtonlistTipoIngreso()),
    ]);
  }
*/

  Widget _fieldDateInitial() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Fecha Inicial'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onChanged: (value) {
            widget._savings.initialDate = value;
          },
        ),
      ],
    );
  }

  Widget _fieldNumberInstallments() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Numero de Cuotas:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de numero de cuotas obligatorio";
            } else {
              return null;
            }
          },
          onChanged: (value) {
            widget._savings.numberInstallment = int.parse(value);
          },
        ),
      ],
    );
  }

  Widget _fieldDateFinal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Fecha Final:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.datetime,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: ""),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la fecha final es obligatorio";
            } else {
              return null;
            }
          },
          onChanged: (value) {
            widget._savings.endDate = value;
          },
        ),
      ],
    );
  }

  Widget _fieldGoal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Meta:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
          onChanged: (value) {
            widget._savings.goal = double.parse(value);
          },
        ),
      ],
    );
  }

  Widget _guardar(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      child: const Text("  Guardar ", style: TextStyle(fontSize: 24)),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          // Guardar los datos en la BD
          try {
            widget._savings.userId = _userId;

            final mess = ScaffoldMessenger.of(context);
            final nav = Navigator.of(context);

            await widget._controller.save(widget._savings);
            mess.showSnackBar(const SnackBar(
                content: Text("Ahorro registrado correctamente")));
            nav.pop();
          } catch (error) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(error.toString())));
          }
        }
      },
    );
  }
}

/*
class DropdownButtonlistDetalle extends StatefulWidget {
  const DropdownButtonlistDetalle({super.key});

  @override
  State<DropdownButtonlistDetalle> createState() =>
      _DropdownButtonlistDetalleState();
}


class _DropdownButtonlistDetalleState extends State<DropdownButtonlistDetalle> {
  String dropdownValue = listDetalle.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: const TextStyle(
          color: Color.fromARGB(255, 0, 0, 0),
          fontSize: 28,
          fontWeight: FontWeight.bold),
      underline: Container(
        height: 0,
        color: const Color.fromARGB(255, 0, 34, 145),
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownValue = value!;
        });
      },
      items: listDetalle.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
*/
/*
class DropdownButtonlistTipoIngreso extends StatefulWidget {
  const DropdownButtonlistTipoIngreso({super.key});

  @override
  State<DropdownButtonlistTipoIngreso> createState() =>
      _DropdownButtonlistTipoIngresoState();
}

class _DropdownButtonlistTipoIngresoState
    extends State<DropdownButtonlistTipoIngreso> {
  String dropdownValue = listTipoIngreso.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: const TextStyle(
          color: Color.fromARGB(255, 0, 0, 0),
          fontSize: 28,
          fontWeight: FontWeight.bold),
      underline: Container(
        height: 0,
        color: const Color.fromARGB(255, 0, 34, 145),
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownValue = value!;
        });
      },
      items: listTipoIngreso.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
*/

/*
///// Checkbox
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({super.key});

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return const Color.fromARGB(255, 24, 0, 245);
    }

    return Checkbox(
      checkColor: Colors.white,
      fillColor: MaterialStateProperty.resolveWith(getColor),
      value: isChecked,
      onChanged: (bool? value) {
        setState(() {
          isChecked = value!;
        });
      },
    );
  }
}
*/