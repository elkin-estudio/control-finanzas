import 'package:app_control_finanzas/controller/fixed_income.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/model/entity/fixed_income.dart';
import 'package:flutter/material.dart';
import '../widgets/drawer.dart';

class principalPaymentPage extends StatefulWidget {
  late final FixedIncomeEntity _fixedIncome;
  late final FixedIncomeController _controller;

  late final _txtPeriodicidad = TextEditingController(text: "0");

  principalPaymentPage({super.key}) {
    _fixedIncome = FixedIncomeEntity();
    _controller = FixedIncomeController();
  }

  @override
  State<principalPaymentPage> createState() => _principalPaymentPageState();
}

class _principalPaymentPageState extends State<principalPaymentPage> {
  final _preferences = SharedPreferences.getInstance();
  String _userId = "";

  @override
  void initState() {
    super.initState();

    _preferences.then((preferences) {
      setState(() {
        _userId = preferences.getString("uid") ?? "N/A";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 2),
          child: Column(children: <Widget>[
            AppBar(
              centerTitle: true,
              title: const Text("Control Finanzas"),
              backgroundColor: const Color.fromARGB(255, 0, 116, 170),
            ),
            const SizedBox(
              height: 50,
            ),
            const Text(
                style: TextStyle(fontSize: 36),
                textAlign: TextAlign.left,
                'Pago principal'),
            Padding(
              padding: const EdgeInsets.all(40.0),
              child: Column(
                children: [
                  _form(context),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _paymentType(context),
          const SizedBox(height: 8),
          _fieldPaymentConsigned(),
          const SizedBox(height: 8),
          //_additionalFields(),
          //const SizedBox(height: 8),
          _fieldPeriodicity(),
          const SizedBox(height: 8),
          ElevatedButton(
            child: const Text("Modificar", style: TextStyle(fontSize: 24)),
            onPressed: () async {},
          ),
          _guardar(context, formKey),
        ],
      ),
    );
  }

  Widget _paymentType(BuildContext context) {
    var opciones = <String>["Diario", "Semanal", "Quincenal", "Mensual"];
    var valor = opciones[1];
    widget._fixedIncome.paymentPeriodicity = opciones[1];
    widget._fixedIncome.paymentPeriodicity = opciones[1];
    return SizedBox(
      height: 80,
      child: DropdownButtonFormField(
        value: valor,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Periodicidad',
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (value) {
          widget._fixedIncome.paymentPeriodicity = value;

          _calculatePeriodicity(context);
        },
      ),
    );
  }

  Widget _fieldPaymentConsigned() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Valor Consignado:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "\$620000"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._fixedIncome.consignedValue = double.tryParse(value!);
          },
        ),
      ],
    );
  }

  Widget _additionalFields() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Detalle:',
            )),
        TextFormField(
          maxLength: 50,
          keyboardType: TextInputType.number,
          obscureText: false,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "Pago Quincenal"),
          onSaved: (value) {
            widget._fixedIncome.detail = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldPeriodicity() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Periodicidad',
            )),
        TextFormField(
          maxLength: 5,
          keyboardType: TextInputType.number,
          obscureText: false,
          controller: widget._txtPeriodicidad,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "15"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._fixedIncome.peridocity = int.tryParse(value!);
          },
        ),
      ],
    );
  }

  Widget _guardar(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      child: const Text("  Guardar ", style: TextStyle(fontSize: 24)),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          // Guardar los datos en la BD
          try {
            widget._fixedIncome.userId = _userId;

            final mess = ScaffoldMessenger.of(context);
            final nav = Navigator.of(context);

            await widget._controller.save(widget._fixedIncome);
            mess.showSnackBar(const SnackBar(
                content: Text("Pago principal registrado correctamente")));
            nav.pop();
          } catch (error) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(error.toString())));
          }
        }
      },
    );
  }

  void _calculatePeriodicity(BuildContext context) {
    var periodicity = widget._fixedIncome.paymentPeriodicity;

    widget._controller.calculatePeriodicity(periodicity!).then((value) {
      widget._txtPeriodicidad.text = value.toString();
    }).onError((error, stackTrace) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("$error"),
      ));
    });
  }
}
