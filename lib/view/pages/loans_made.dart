import '/view/widgets/drawer.dart';
import 'package:flutter/material.dart';

const List<String> listDetalle = <String>[
  'Le preste a Pedro Enrique',
  'Le preste a Carlos Gonzales',
];

class LoansMadePage extends StatelessWidget {

  const LoansMadePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          AppBar(
            centerTitle: true,
            title: const Text("Control Finanzas"),
            backgroundColor: const Color.fromARGB(255, 0, 116, 170),
          ),
          Padding(
              padding: const EdgeInsets.all(40.0),
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                      style: TextStyle(fontSize: 36),
                      textAlign: TextAlign.left,
                      'Prestamos Realizados'),
                  const SizedBox(
                    height: 20,
                  ),
                  _paymentDetail(),
                  _fieldValue(),
                  _fieldDateInitial(),
                  _fieldNumberInstallments(),
                  _fieldDateFinal(),
                  _fieldInterests(),
                  _fieldValueInterests(),
                  _fieldTotal(),
                ],
              )),
          Padding(
              padding: const EdgeInsets.all(0.0),
              child: Column(children: [
                ElevatedButton(
                  child:
                      const Text("  Nuevo   ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text("Modificar", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text(" Eliminar  ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text(" Guardar  ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
              ]))
        ]),
      ),
    );
  }

  Widget _paymentDetail() {
    return Column(children: [
      const TextField(
          enabled: false,
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            hintText: 'Detalle:',
          )),
      Container(
          alignment: Alignment.bottomLeft,
          child: const DropdownButtonlistDetalle()),
    ]);
  }

  Widget _fieldValue() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Valor:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _fieldDateInitial() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Fecha de Inicio'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onSaved: (value) {},
        ),
      ],
    );
  }

  Widget _fieldNumberInstallments() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Numero de Cuotas:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _fieldDateFinal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Fecha Final:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _fieldInterests() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Intereses:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _fieldValueInterests() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Valor Intereses:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _fieldTotal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Total:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }
}

class DropdownButtonlistDetalle extends StatefulWidget {
  const DropdownButtonlistDetalle({super.key});

  @override
  State<DropdownButtonlistDetalle> createState() =>
      _DropdownButtonlistDetalleState();
}

class _DropdownButtonlistDetalleState extends State<DropdownButtonlistDetalle> {
  String dropdownValue = listDetalle.first;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      elevation: 16,
      style: const TextStyle(
          color: Color.fromARGB(255, 0, 0, 0),
          fontSize: 28,
          fontWeight: FontWeight.bold),
      underline: Container(
        height: 0,
        color: const Color.fromARGB(255, 0, 34, 145),
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownValue = value!;
        });
      },
      items: listDetalle.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
