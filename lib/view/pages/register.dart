import 'dart:io';

import '/view/pages/take_photo.dart';
import 'package:camera/camera.dart';

import '../widgets/photo_avatar.dart';
import '/controller/login.dart';
import '/controller/request/register.dart';
import 'package:flutter/material.dart';

const List<String> listGenero = <String>[
  'Masculino',
  'Femenino',
  'Prefieron no decirlo'
];

class RegisterPage extends StatefulWidget {
  late RegisterRequest _data;
  late LoginController _controller;

  RegisterPage({super.key}) {
    _data = RegisterRequest();
    _controller = LoginController();
  }

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String? _foto;

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AppBar(
              centerTitle: true,
              title: const Text("Control Finanzas"),
              backgroundColor: const Color.fromARGB(255, 0, 116, 170),
            ),
            Padding(
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    _form(context),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<
        FormState>(); // registramos el formulario en flutter para Habilitar validaciones especiales
    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          children: [
            const Text(
                style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
                'Crear Cuenta'),
            const SizedBox(
              height: 40,
            ),
            _fieldIcon(context),
            const SizedBox(
              height: 25,
            ),
            _fieldName(),
            _fieldLastName(),
            _fieldEmailAddress(),
            _fieldPassword(),
            const SizedBox(
              height: 25,
            ),
            _fieldCountry(),
            _address(),
            _fieldNumberPhone(),
            const SizedBox(
              height: 25,
            ),
            _fieldGender(context),
            const SizedBox(
              height: 50,
            ),
            _fieldDate(),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              child: const Text("Crear Cuenta", style: TextStyle(fontSize: 24)),
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  // Validar usuario y contraseña en base de datos
                  try {
                    await widget._controller.registerNewUser(widget._data);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text("Usuario registrado correctamente")));
                    Navigator.pop(context);
                  } catch (error) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text(error.toString())));
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _fieldIcon(BuildContext context) {
    Widget icono;
    if (_foto == null) {
      icono = IconButton(
        icon: const CircleAvatar(radius: 100, child: photoAvatarWidget()),
        color: Colors.blue,
        onPressed: () async {
          var nav = Navigator.of(context);

          final cameras = await availableCameras();
          final camera = cameras.first;

          var imagePath = await nav.push<String>(
            MaterialPageRoute(
              builder: (context) => TakePhotoPage(camera: camera),
            ),
          );

          if (imagePath != null && imagePath.isNotEmpty) {
            setState(() {
              _foto = imagePath;
              widget._data.photo = imagePath;
            });
          }
        },
      );
    } else {
      icono = CircleAvatar(
        radius: 100,
        backgroundImage: FileImage(File(_foto!)),
      );
    }

    return icono;
  }

/*
  Widget _fieldIcon(BuildContext context) {
    Widget component;
     if(_foto == null){
      icono =
     }
     = Material(
      color: Colors.white,
      child: Center(
        child: Ink(
          decoration: const ShapeDecoration(
            color: Colors.lightBlue,
            shape: CircleBorder(),
          ),
          child: IconButton(
            icon: const photoAvatarWidget(),
            color: Colors.white,
            onPressed: () async {
              var nav = Navigator.of(context);

              final cameras = await availableCameras();
              final camera = cameras.first;

              var imagePath = await nav.push<String>(
                MaterialPageRoute(
                  builder: (context) => TakePhotoPage(camera: camera),
                ),
              );

              if (imagePath != null && imagePath.isNotEmpty) {
                setState(() {});
              } else {}
            },
          ),
        ),
      ),
    );
    return component;
  }*/

  Widget _fieldName() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Nombre'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El campo nombre es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._data.name = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldLastName() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Apellidos'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El campo apellidos es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._data.lastName = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldEmailAddress() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Correo Electronico'),
        ),
        TextFormField(
          maxLength: 30,
          keyboardType: TextInputType.emailAddress,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El correo electronico es obligatorio";
            }
            if ((!value.contains("@")) || (!value.contains("."))) {
              return "El correo electronico no tiene un formato valido";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._data.email = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldPassword() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Contraseña'),
        ),
        TextFormField(
          obscureText: true,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "La contraseña es obligatoria";
            }
            if (value.length < 8) {
              return "Minimo debe contener 8 caracteres";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._data.password = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldCountry() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Pais'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El campo pais es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._data.country = value!;
          },
        ),
      ],
    );
  }

  Widget _address() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Direccion'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onSaved: (value) {
            widget._data.address = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldNumberPhone() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: SizedBox(
                width: 100,
                child: Container(
                  alignment: Alignment.bottomLeft,
                  child: const Text(
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.start,
                      'Indicativo'),
                ),
              ),
            ),
            const SizedBox(
              width: 25,
            ),
            Flexible(
              child: Container(
                alignment: Alignment.bottomLeft,
                child: const Text(
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.start,
                    'Telefono'),
              ),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: SizedBox(
                width: 100,
                child: TextFormField(
                  maxLength: 10,
                  obscureText: false,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "+57"),
                  validator: (value) {},
                  onSaved: (value) {
                    widget._data.indicativePhone = value!;
                  },
                ),
              ),
            ),
            const SizedBox(
              width: 25,
            ),
            Flexible(
              child: TextFormField(
                maxLength: 20,
                obscureText: false,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: "Telefono"),
                validator: (value) {},
                onSaved: (value) {
                  widget._data.phone = value!;
                },
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget _fieldGender(BuildContext context) {
    var opciones = <String>["Prefiero no decirlo", "Masculino", "Femenino"];
    var valor = opciones[0];
    widget._data.gender = opciones[0];

    return SizedBox(
      height: 80,
      child: DropdownButtonFormField(
        value: valor,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Genero',
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (value) {
          widget._data.gender = value;
        },
      ),
    );
  }

  Widget _fieldDate() {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomLeft,
          child: const Text(
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.start,
              'Fecha Nacimiento'),
        ),
        TextFormField(
          maxLength: 100,
          obscureText: false,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
          ),
          validator: (value) {},
          onSaved: (value) {
            widget._data.birthDay = value!;
          },
        ),
      ],
    );
  }
}
