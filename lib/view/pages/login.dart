import '/view/pages/recover_account.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/view/pages/register.dart';

import '/controller/login.dart';
import 'package:flutter/material.dart';
import '../../controller/request/login.dart';
import 'principal.dart';

class LoginPage extends StatelessWidget {
  final _preferences = SharedPreferences.getInstance();
  final _imageUrl = "assets/images/CFinanzas.jpg";
  late LoginController _controller;
  late LoginRequest _request;

  LoginPage({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: <Widget>[
              AppBar(
                  centerTitle: true,
                  title: const Text("Control Finanzas"),
                  backgroundColor: const Color.fromARGB(255, 0, 116, 170),
                  leading: IconButton(
                    icon: const Icon(Icons.account_balance),
                    onPressed: () {},
                  )),
              _logo(),
              _form(context),
              _forgotPassword(context),
              _createAcount(context),
              const SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        Image.asset(_imageUrl), //Importamos,
        const SizedBox(
          height: 25,
        ),
      ],
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<
        FormState>(); // registramos el formulario en flutter para Habilitar validaciones especiales
    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          children: [
            _fieldEmailAddress(),
            const SizedBox(
              height: 25,
            ),
            _fieldPassword(),
            const SizedBox(
              height: 40,
            ),
            _buttonStartSession(context, formKey),
          ],
        ),
      ),
    );
  }

  Widget _fieldEmailAddress() {
    return TextFormField(
      maxLength: 30,
      keyboardType: TextInputType.emailAddress,
      obscureText: false,
      decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Correo electronico o Num. Celular"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El correo electronico es obligatorio";
        }
        if ((!value.contains("@")) || (!value.contains("."))) {
          return "El correo electronico no tiene un formato valido";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _request.email = value!;
      },
    );
  }

  Widget _fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
          border: OutlineInputBorder(), labelText: "Contraseña"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La contraseña es obligatoria";
        }
        if (value.length < 8) {
          return "Minimo debe contener 8 caracteres";
        } else {
          return null;
        }
      },
      onSaved: (value) {
        _request.password = value!;
      },
    );
  }

  Widget _forgotPassword(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 5,
        ),
        GestureDetector(
          child: const Text(
              style: TextStyle(
                fontSize: 18,
                color: Color.fromARGB(255, 2, 104, 172),
                decoration: TextDecoration.underline,
              ),
              textAlign: TextAlign.center,
              '¿Olvidaste tu contraseña?'),
          onTap: () {
            // Validar usuario y contraseña en base de datos
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const RecoverAccount(),
              ),
            );
          },
        ),
        const SizedBox(
          height: 50,
        ),
      ],
    );
  }

  Widget _createAcount(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: const Text(
              style: TextStyle(
                fontSize: 18,
                color: Color.fromARGB(255, 2, 104, 172),
                decoration: TextDecoration.underline,
              ),
              textAlign: TextAlign.center,
              'Crear cuenta'),
          onTap: () {
            // Validar usuario y contraseña en base de datos
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => RegisterPage(),
              ),
            );
          },
        ),
        const SizedBox(
          height: 50,
        ),
      ],
    );
  }

  Widget _buttonStartSession(
      BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      child: const Text("Iniciar Sesion", style: TextStyle(fontSize: 24)),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          // Validar usuario y contraseña en base de datos

          try {
            final nav = Navigator.of(context);
            var userInfo = await _controller.validateEmailPassword(_request);

            var pref = await _preferences;
            pref.setString("uid", userInfo.id!);
            pref.setString("email", userInfo.email!);
            pref.setString("name", userInfo.name!);
            pref.setString("photo", userInfo.photo!);

            nav.push(
              MaterialPageRoute(
                builder: (context) => const PrincipalPage(),
              ),
            );
          } catch (e) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(e.toString())));
          }
        }
      },
    );
  }
}
