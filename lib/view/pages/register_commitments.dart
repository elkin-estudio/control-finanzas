import 'package:app_control_finanzas/view/pages/principal.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/commitments.dart';
import '../../model/entity/commitments.dart';
import '/view/widgets/drawer.dart';
import 'package:flutter/material.dart';

class RegisterCommitmentsPage extends StatefulWidget {
  late final CommitmentsEntity _commitments;
  late final CommitmentsController _controller;

  RegisterCommitmentsPage({
    super.key,
  }) {
    _commitments = CommitmentsEntity();
    _controller = CommitmentsController();
  }

  @override
  State<RegisterCommitmentsPage> createState() =>
      _RegisterCommitmentsPageState();
}

class _RegisterCommitmentsPageState extends State<RegisterCommitmentsPage> {
  final _preferences = SharedPreferences.getInstance();
  String _userId = "";

  @override
  void initState() {
    super.initState();

    _preferences.then((preferences) {
      setState(() {
        _userId = preferences.getString("uid") ?? "N/A";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          AppBar(
            centerTitle: true,
            title: const Text("Control Finanzas"),
            backgroundColor: const Color.fromARGB(255, 0, 116, 170),
          ),
          const SizedBox(
            height: 50,
          ),
          const Text(
              style: TextStyle(fontSize: 36),
              textAlign: TextAlign.left,
              'Compromisos'),
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: Column(
              children: [
                _form(context),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(0.0),
              child: Column(children: [
                ElevatedButton(
                  child:
                      const Text("   Nuevo   ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text("Modificar", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
                ElevatedButton(
                  child:
                      const Text("  Eliminar ", style: TextStyle(fontSize: 24)),
                  onPressed: () async {},
                ),
              ]))
        ]),
      ),
    );
  }

  Widget _form(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _paymentDetail(),
          _fieldPaymentConsigned(),
          const SizedBox(
            height: 20,
          ),
          _paymentType(context),
          const SizedBox(
            height: 20,
          ),
          _fieldDateInitial(),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: const [
              MyStatefulWidget(),
              Text(
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                  'Fecha Final Pago'),
            ],
          ),
          _fieldInstallmentNumber(),
          _fieldDateFinal(),
          _guardar(context, formKey),
        ],
      ),
    );
  }

  Widget _paymentDetail() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Detalle de la obligacion:',
            )),
        TextFormField(
          maxLength: 50,
          keyboardType: TextInputType.text,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "Factura Celular"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El campo detalle es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._commitments.detail = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldPaymentConsigned() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Valor Compromiso:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "\$35000"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor del compromiso es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._commitments.commitmentValue = double.tryParse(value!);
          },
        ),
      ],
    );
  }

  Widget _fieldDateInitial() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Fecha de corte',
            )),
        TextFormField(
          maxLength: 10,
          keyboardType: TextInputType.number,
          obscureText: false,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "12/11/2022"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._commitments.courtDate = value!;
          },
        ),
      ],
    );
  }

  /*Widget _paymentType() {
    return Column(children: [
      const TextField(
          enabled: false,
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            hintText: 'Tipo de valor:',
          )),
      Container(
          alignment: Alignment.bottomLeft,
          child: const DropdownButtonPeriodiciadaPago()),
    ]);
  }*/
  Widget _paymentType(BuildContext context) {
    var opciones = <String>["Fijo", "Variable"];
    var valor = opciones[0];
    widget._commitments.valueType = opciones[0];

    return SizedBox(
      height: 80,
      child: DropdownButtonFormField(
        value: valor,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Tipo de Valor',
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (value) {
          widget._commitments.valueType = value;
        },
      ),
    );
  }

  Widget _fieldInstallmentNumber() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Numero de Cuotas:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._commitments.numberInstallments = int.tryParse(value!);
          },
        ),
      ],
    );
  }

  Widget _fieldDateFinal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Fecha final',
            )),
        TextFormField(
          maxLength: 10,
          keyboardType: TextInputType.number,
          obscureText: false,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "12/11/2022"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
          onSaved: (value) {
            widget._commitments.finalDate = value!;
          },
        ),
      ],
    );
  }

  Widget _fieldTotal() {
    return Column(
      children: [
        const TextField(
            enabled: false,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              hintText: 'Total:',
            )),
        TextFormField(
          maxLength: 12,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
              border: OutlineInputBorder(), labelText: "0"),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El valor de la consignacion es obligatorio";
            } else {
              return null;
            }
          },
          /*onSaved: (value) {
            _commitments.total = DateTime.tryParse(value!);
          },*/
        ),
      ],
    );
  }

  Widget _guardar(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      child: const Text("  Guardar ", style: TextStyle(fontSize: 24)),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          // Guardar los datos en la BD
          try {
            widget._commitments.userId = _userId;
            await widget._controller.save(widget._commitments);
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text("Obligacion registrada correctamente")));
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => const PrincipalPage(),
              ),
            );
          } catch (error) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(error.toString())));
          }
        }
      },
    );
  }
}

///// Checkbox
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({super.key});

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return const Color.fromARGB(255, 24, 0, 245);
    }

    return Checkbox(
      checkColor: Colors.white,
      fillColor: MaterialStateProperty.resolveWith(getColor),
      value: isChecked,
      onChanged: (bool? value) {
        setState(() {
          isChecked = value!;
        });
      },
    );
  }
}
