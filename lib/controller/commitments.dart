import '../model/repository/commitments.dart';
import '/model/entity/commitments.dart';

class CommitmentsController {
  late CommitmentsRepository _repository;

   CommitmentsController()  {
    _repository = CommitmentsRepository();
  }

  Future<void> save(CommitmentsEntity comitments) async {
    await _repository.newCommitments(comitments);
  }

  Future<List<CommitmentsEntity>> listAll(String id) async {
    return await _repository.getAllByUserId(id);
  }
}
