class RegisterRequest {
  late String? name;
  late String? photo;
  late String? lastName;
  late String email;
  late String password;
  late String? country;
  late String? address;
  late String? indicativePhone;
  late String? phone;
  late String? gender;
  late String? birthDay;

  @override
  String toString() {
    return "$name,$lastName,$email,$password,$country,$address,$indicativePhone,$phone,$gender,$birthDay,$photo";
  }
}
