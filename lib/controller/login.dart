import '/controller/request/register.dart';
import '/controller/response/user_info.dart';
import '/model/entity/user.dart';
import '/model/repository/fb_auth.dart';
import '/model/repository/user.dart';
import 'request/login.dart';

class LoginController {
  late final UserRepository _userRepository;
  late final FirebaseAuthenticationRepository _authRepository;

  LoginController() {
    _userRepository = UserRepository();
    _authRepository = FirebaseAuthenticationRepository();
  }

  Future<UserInfoResponse> validateEmailPassword(LoginRequest request) async {
    await _authRepository.signInEmailPassword(request.email, request.password);
    // Consultar usuario con el correo dado
    var user = await _userRepository.findByEmail(request.email);

    return UserInfoResponse(
      id: user.id,
      email: user.email,
      name: user.name,
      photo: user.photo,
    );
  }

  Future<void> registerNewUser(RegisterRequest request) async {
    // Consulto si el usuario ya existe
    try {
      var user = await _userRepository.findByEmail(request.email);
      if (user.email != null) {
        return Future.error(
            "Ya existe un usuario con el correo actual, favor cambie de correo electronico");
      }
    } catch (e) {
      // Crear un correo y clave en la atenticacion en firebase Authentication
      await _authRepository.createEmailPasswordAccount(
          request.email, request.password);

      _userRepository.save(UserEntity(
          name: request.name,
          email: request.email,
          photo: request.photo,
          //password: request.password,
          country: request.country,
          address: request.address,
          indicativePhone: request.indicativePhone,
          phone: request.phone,
          gender: request.gender,
          birthDay: request.birthDay));
      // Agregar informacion adicional en base de datos
    }
  }

  Future<void> logout() async {
    await _authRepository.signOut();
  }
}
