import 'package:app_control_finanzas/model/entity/fixed_income.dart';
import 'package:app_control_finanzas/model/repository/backend.dart';
import 'package:app_control_finanzas/model/repository/fixed_income.dart';

class FixedIncomeController {
  late FixedIncomeRepository _repository;
  late BackendRepository _backendRepository;

  FixedIncomeController() {
    _repository = FixedIncomeRepository();
    _backendRepository = BackendRepository();
  }

  Future<void> save(FixedIncomeEntity fixedIncome) async {
    await _repository.newFixedIncome(fixedIncome);
  }

  Future<int> calculatePeriodicity(String periodicity) async {
    var value = await _backendRepository.CalculatePeriodicity(periodicity);
    return value;
  }
}
