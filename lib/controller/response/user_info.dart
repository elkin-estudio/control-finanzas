class UserInfoResponse {
  late String? id;
  late String? name;
  late String? email;
  late String? photo;

  UserInfoResponse({this.id, this.email, this.name, this.photo});
}
