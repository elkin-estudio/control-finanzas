import '../model/entity/savings.dart';
import '../model/repository/savings.dart';

class SavingsController {
  late SavingsRepository _repository;
  //late BackendRepository _backendRepository;

  SavingsController() {
    _repository = SavingsRepository();
    //_backendRepository = BackendRepository();
  }

  Future<void> save(SavingsEntity savings) async {
    await _repository.newSavings(savings);
  }

  Future<List<SavingsEntity>> listAll(String id) async {
    return await _repository.getAllByUserId(id);
  }
}
